# GOM to VIC-3D converter

## Description
The MATLAB live script *src/GOM_to_VIC3D_converter.mlx* converts the DIC (digital image correlation) results generated with the [GOM Correlate](https://www.gom.com/de-de/produkte/zeiss-quality-suite/gom-correlate-pro) software to the format of the commercial DIC software [VIC-3D](https://www.correlatedsolutions.com/vic-3d/) from Correlated Solutions. 

This is necessary to run [ACDM](https://gitlab.ethz.ch/ibk-kfm-public/acdm/) (automatic crack detection and measurement) on GOM Correlate files. In ACDM, the converted files can then be read as VIC-3D DIC files.

## Details
GOM Correlate uses a triangular grid of measuring points. ACDM, however, runs only with data in a square grid. The main function of this converter is the rearrangement of the triangular grid from GOM Correlate to a square grid of data points required for ACDM. 

While running the code, the square spacing of the final data grid has to be set. The default value for this spacing is proposed in the converter as follows:<br>
A Delaunay triangulation is performed on the data points exported from GOM Correlate based on their position in the xy-space. The side lengths of a specified number of randomly selected triangles are calculated. The default value of the square spacing is the median of these side lengths.

 <img src="assets/images/spacing.PNG"  width="500"><br><br>

In addition, the spacing of these nodes (in pixels) needs to be chosen while running the code. This pixel spacing is automatically discerned as the step size in ACDM, which is used for the calculation of the default separation distance between the reference points used for the calculation of the crack kinematics. 

 <img src="assets/images/pixelspacing.PNG"  width="500"><br><br>

The data values (z-coordinate, displacements, principal strain, angle of principal strain) at the nodes of the square grid are found by linear interpolation on the triangulated GOM data.<br>

In order to have reliable data, a maximum interpolation distance between the node in the square grid and the nodes of the GOM data used for the linear interpolation needs to be chosen. The default value given for this maximum interpolation distance (in mm) is calculated as a multiple of the median of the side lengths of randomly chosen triangles as described above. The data at the nodes of the square grid whose values are found by extrapolation or where at least one of the three distances to the GOM data used for the linear interpolation is larger than the maximum tolerable distance are not considered in the ACDM analysis.

 <img src="assets/images/distmax.PNG"  width="500"><br><br>

## Prerequisites
The converter was written in Matlab R2022a.

The data output from GOM Correlate must be formatted in the following way:
-   Each measuring step needs to be saved as a separate .csv-file.
-   In each .csv file, each measuring point needs to be written in a separate row
-   The columns of each .csv file need to contain the following data:
    identity [-] | x [mm] | y [mm] | z [mm] |  maximum principal strain [%] | minimum principal strain [%] | principal strain orientation [rad]

## Usage
1.  Run *GOM_to_VIC3D_converter.m* in a MATLAB instance
    ```bash
    >> GOM_to_VIC3D_converter
    ```
2.  Follow the instructions given by the prompts.

## Author
This converter is developed by [Rebecca Ammann](https://kaufmann.ibk.ethz.ch/people/staff/rebecca-ammann.html) at the [Chair of Structural Engineering - Concrete Structures and Bridge Design, ETH Zürich](https://kaufmann.ibk.ethz.ch/)

## Copyright and license
The MATLAB script is licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) (the "License")
